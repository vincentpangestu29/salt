import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

// Image
import logo from "../assets/company-logo.png";
import banner from "../assets/banner.png";
import bottomIcon from "../assets/bottom-icon.png";

// Style
import classes from "./Headers.module.css";

const Headers = () => {
  return (
    <>
      <header className="align-items-center d-flex justify-content-between p-3">
        <div className="d-flex align-items-center">
          <img src={logo} className="" alt="logo" />
          <b className={classes["company-name"]}>COMPANY</b>
        </div>
        <FontAwesomeIcon icon={faBars} />
      </header>
      <section className="d-flex flex-wrap position-relative">
        <div className="col-md-8 col-12">

        <img src={banner} className="w-100" alt="banner" />
        </div>
        <div className={`col-md-4 col-12 p-5 ${classes["banner-container"]}`}>
          <h1 className="mb-3">Discover Your Wonder</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse
          </p>
        </div>
        <div className={classes['bottom-icon']}>
          <img src={bottomIcon} className="" alt="Bottom Icon" />
        </div>
      </section>
    </>
  );
};

export default Headers;
