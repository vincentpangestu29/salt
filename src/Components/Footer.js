const Footer = () => {
  return (
    <section className="d-flex flex-wrap justify-content-between p-3 footer" style={{ background: "#07477A" }}>
      <div className="bg-white p-3 col-12 col-md-6">
        <select className="w-100 mb-4 form-control">
          <option>TECHNOLOGY DEPARTMENT</option>
        </select>
        <span className="text-info">
          Jl. Lembong No 8 Kel. Braga Kec. Sumur Bandung, Kota Bandung, Jawa
          Barat
        </span>
      </div>
      <div className="col-12 col-md-1">
        <a
          className="d-flex mt-3 text-white"
          href="#profile"
          style={{ textDecoration: "none" }}
        >
          Who We Are
        </a>
        <a
          className="d-flex mt-3 text-white"
          href="#value"
          style={{ textDecoration: "none" }}
        >
          Our Values
        </a>
        <a
          className="d-flex mt-3 text-white"
          href="#specialty"
          style={{ textDecoration: "none" }}
        >
          The Perks
        </a>
      </div>
    </section>
  );
};

export default Footer;
