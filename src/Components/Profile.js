import { useState } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons";

import classes from "./Profile.module.css";

const dummyData = [
  {
    id: "01",
    title: "Who we are",
    subtitle: "Technology Company",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse",
  },
  {
    id: "02",
    title: "What we do",
    subtitle: "Professional Brand Management",
    description:
      "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit",
  },
  {
    id: "03",
    title: "How we do",
    subtitle: "Strategize, Design, Collaborate",
    description:
      "sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, a",
  },
];

const Profile = () => {
  const [content, setContent] = useState({
    id: dummyData[0].id,
    title: dummyData[0].title,
    subtitle: dummyData[0].subtitle,
    description: dummyData[0].description,
  });

  const getIndex = dummyData.findIndex((currentContent) => {
    return currentContent.id === content.id;
  });

  const prevNav = () => {
    if (getIndex !== 0) {
      setContent({
        id: dummyData[getIndex - 1].id,
        title: dummyData[getIndex - 1].title,
        subtitle: dummyData[getIndex - 1].subtitle,
        description: dummyData[getIndex - 1].description,
      });
    }
  };

  const nextNav = () => {
    if (getIndex !== dummyData.length - 1) {
      setContent({
        id: dummyData[getIndex + 1].id,
        title: dummyData[getIndex + 1].title,
        subtitle: dummyData[getIndex + 1].subtitle,
        description: dummyData[getIndex + 1].description,
      });
    }
  };

  return (
    <section id="profile" className="p-5">
      <h2 className={classes["text-blue"] + " mb-3"}>{content.title}</h2>
      <span style={{ fontWeight: 500 }}>{content.subtitle}</span>
      <p className="mt-2">{content.description}</p>
      <div className="d-flex justify-content-between align-items-center mt-5">
        <div>
          <b style={{ fontSize: "21px" }}>{content.id}</b>
          <b className="text-secondary" style={{ color: "#b7b1b1 !important" }}>
            {" "}
            / 03
          </b>
        </div>
        <div>
          <button
            type="button"
            onClick={prevNav}
            className={`${classes.btnNav} ${classes.btnPrev}`}
          >
            <FontAwesomeIcon icon={faArrowLeft} />
          </button>
          <button
            type="button"
            onClick={nextNav}
            className={`${classes.btnNav} ${classes.btnNext}`}
          >
            <FontAwesomeIcon icon={faArrowRight} />
          </button>
        </div>
      </div>
    </section>
  );
};

export default Profile;
