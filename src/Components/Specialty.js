import React from "react";

import { Navigation, Pagination, Scrollbar, A11y } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons";

// Images
import accesories from "../assets/accesories.png";
import exhaust from "../assets/exhaust.png";
import speed from "../assets/speed.png";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

import classes from "./Specialty.module.css";

const dummy = [
  {
    id: 1,
    image: accesories,
    label: "Accesories",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et",
  },
  {
    id: 2,
    image: speed,
    label: "Speed Improvement",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et",
  },
  {
    id: 3,
    image: exhaust,
    label: "Exhaust",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et",
  },
];

const Specialty = () => {
  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);

  const sliderContent = dummy.map((item) => (
    <SwiperSlide key={item.id}>
      <div className="text-center">
        <img src={item.image} />
        <br />
        <b>{item.label}</b>
        <p className="mt-3">{item.description}</p>
      </div>
    </SwiperSlide>
  ));

  return (
    <section id="specialty" className={`${classes["section-container"]} p-4`}>
      <div className="p-4" style={{ background: "#F5F5F5" }}>
        <h2 className={classes["text-blue"]}>OUR SPECIALTY</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et
        </p>
        <div className={classes["custom-swiper"]}>
          <Swiper
            modules={[Navigation, Pagination, Scrollbar, A11y]}
            spaceBetween={50}
            slidesPerView={1}
            onSwiper={(swiper) => {
              setTimeout(() => {
                swiper.params.navigation.prevEl = navigationPrevRef.current;
                swiper.params.navigation.nextEl = navigationNextRef.current;
                swiper.navigation.destroy();
                swiper.navigation.init();
                swiper.navigation.update();
              });
            }}
            pagination={{ clickable: true }}
          >
            {sliderContent}
            <button
              type="button"
              className={`${classes["custom-navigation"]} ${classes.prev}`}
              ref={navigationPrevRef}
            >
              <FontAwesomeIcon icon={faArrowLeft} />
            </button>
            <button
              type="button"
              className={`${classes["custom-navigation"]} ${classes.next}`}
              ref={navigationNextRef}
            >
              <FontAwesomeIcon icon={faArrowRight} />
            </button>
          </Swiper>
        </div>
      </div>
    </section>
  );
};

export default Specialty;
