import banner from "../assets/banner-profile.png";
import classes from "./CoreValues.module.css";

const dummyData = [
  {
    id: 1,
    title: "Dedication",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et ",
  },
  {
    id: 2,
    title: "Intellectual Honesty",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et ",
  },
  {
    id: 3,
    title: "Curiosity",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et ",
  },
];
const CoreValues = () => {
  return (
    <div className="bg-light d-flex flex-wrap align-items-center">
      <section id="value" className="p-5 col-md-8 col-12">
        <h2 className={classes["text-blue"]}>Our Core Values</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse
        </p>
        {dummyData.map((dummy) => (
          <div className="d-flex mt-2" key={dummy.id}>
            <div className={classes["brd-list"]}></div>
            <div>
              <b>{dummy.title}</b>
              <p>{dummy.content}</p>
            </div>
          </div>
        ))}
      </section>
      <div className="col-12 col-md-4">
        <img src={banner} className="w-100" alt="Banner" />
      </div>
    </div>
  );
};

export default CoreValues;
