import Headers from './Components/Headers';
import Profile from './Components/Profile';
import CoreValues from './Components/CoreValues';
import Specialty from './Components/Specialty';
import Footer from './Components/Footer';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  return (
    <div className="App">
      <Headers />
      <Profile />
      <CoreValues />
      <Specialty />
      <Footer />
    </div>
  );
}

export default App;
